<?php

session_start();


$money = [
    'uah' => [
        'name' => 'Гривне',
        'course' => 1,
    ],
    'usd' => [
        'name' => 'Долларах',
        'course' => 27.1,
    ],
    'eur' => [
        'name' => 'Евро',
        'course' => 30.2,
    ],
];

$goods = [
    [
        'title' => 'Конфета',
        'price_val' => '20',
    ],
    [
        'title' => 'Шоколад',
        'price_val' => '25',
    ],
    [
        'title' => 'Шампунь',
        'price_val' => '50',
    ],
    [
        'title' => 'Зубная паста',
        'price_val' => '60',
    ],
    [
        'title' => 'Зубная счетка',
        'price_val' => '40',
    ],
    [
        'title' => 'Апельсин',
        'price_val' => '10',
    ],
    [
        'title' => 'Гранат',
        'price_val' => '50',
    ],
    [
        'title' => 'Стул',
        'price_val' => '500',
    ],
    [
        'title' => 'Чашка',
        'price_val' => '60',
    ],
    [
        'title' => 'Набор чашек',
        'price_val' => '250',
    ],
];
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    
<div class="container">
    <div class="row col-12">
        <form action="/currency.php" method="GET">
            <div class="form-group">
                <label>Выберите валюту:
                    <select multiple class="form-control" name=currency>
                        <option value='uah'>Гривна</option>
                        <option value='usd'>Доллар</option>
                        <option value='eur'>Евро</option>
                    </select>
                </label>
            </div>
                <div class="form-group">
                    <button class="btn btn-success">push</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="container">
    <div class="row col">
        <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Стоимость в 
                        <?php if ($_SESSION['currency'] == 'uah'):
                            echo $money['uah']['name'];
                            elseif($_SESSION['currency'] == 'usd'):
                                echo $money['usd']['name'];
                            elseif($_SESSION['currency'] == 'eur'):
                                echo $money['eur']['name'];
                            endif
                        ?>
                    </th>
                </tr>
            <?php foreach($goods as $key => $good):?>
                <tr>
                    <td><?=++$key?></td>
                    <td><?=$good['title']?></td>
                    <?php if ($_SESSION['currency'] == 'uah'):?>
                    <td><?=$good['price_val'] / $money['uah']['course']?></td>
                    <?php elseif ($_SESSION['currency'] == 'usd'):?>
                    <td><?=$good['price_val'] / $money['usd']['course']?></td>
                    <?php else :($_SESSION['currency'] == 'eur')?>
                    <td><?=$good['price_val'] / $money['eur']['course']?></td>
                    <?php endif?>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
</div>   


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>